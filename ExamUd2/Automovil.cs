﻿using System.Reflection;
using System.Text.RegularExpressions;

internal class Automovil : Vehiculo
{
    private int anio;

    public Automovil(string marca, string modelo,int anio) : base(marca, modelo){
        this.anio = anio;
    }

    override public void mostrarInformacion()
    {
        Console.WriteLine($"Automovil: " +
            $"\n\tmarca: {getMarca()} " +
            $"\n\tmodelo: {getModelo()} " +
            $"\n\tanio: {anio}");
    }
}

