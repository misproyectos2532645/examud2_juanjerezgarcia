﻿internal class Calculadora{
    public void suma(int x,int y) {
        Console.WriteLine($"{x} + {y}= {x+y}");
    }

    public void resta(int x, int y)
    {
        Console.WriteLine($"{x} - {y}= {x - y}");
    }

    public void multiplicacion(int x, int y)
    {
        Console.WriteLine($"{x} * {y}= {x * y}");
    }

    public void division(int x, int y)
    {
        if (y == 0)
        {
            Console.WriteLine($"Esta operación {x}/{y} no se puede realizar ya que \"y\" es 0");
        }
        else {
            Console.WriteLine($"{x} / {y}= {x / y}");
        }
    }
}

