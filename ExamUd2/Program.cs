﻿//Creador Juan Jerez García 2ºDAM
internal class Program{
    private static void Main(string[] args){
        string opcion = "";
        do
        {
            Console.WriteLine("\n0. Salir\r\n1. manipulación de array (Suma Array)\r\n2. clases herencia (Vehiculos - Automovil) \r\n3. manejo de exepciones (calculadora) \r\n4. manipulación cadena (cadena orden inverso) \r\n5. Manipulación de Cadenas y Expresiones Regulares (palabra palíndromo)");

            opcion = pideCadena("Introducir una opción (0-5): ");
            seleccionarOpcion(opcion);

        } while (!opcion.Equals("0"));
    }

    private static void seleccionarOpcion(string opcion)
    {
        switch (opcion)
        {
            case "0":
                Console.WriteLine("saliendo del programa");
                break;
            case "1":
                int[] enteros= {1,1,1,1,1};
                apartado1(enteros);
                break;
            case "2":
                apartado2();
                break;
            case "3":
                apartado3();
                break;
            case "4":
                apartado4();
                break;
            case "5":
                apartado5();
                break;
            default:
                Console.WriteLine("Opción no valida");
                break;
        }
    }

    //apartado 1
    private static void apartado1(int[] enteros)
    {
        if (enteros.Length > 0){
            int suma = 0;

            for (int i = 0; i < enteros.Length; i++){
                suma += enteros[i];
            }
            Console.WriteLine(suma);
        }
        else{
            Console.WriteLine("el array esta vacio");
        }
        
    }

    //apartado 2
    private static void apartado2()
    {
        Vehiculo vehiculo = new Vehiculo("Toyota","B-500");
        Automovil automovil = new Automovil("Audi","C-200",2005);

        vehiculo.mostrarInformacion();
        automovil.mostrarInformacion();
    }

    //apartado 3
    private static void apartado3()
    {
        Calculadora calculadora = new Calculadora();
        //estos metodos te obligan a colocar un número correcto
        int x = pideEntero("introducir x: ");
        int y = pideEntero("introducir y: ");

        calculadora.suma(x,y);
        calculadora.resta(x, y);
        calculadora.multiplicacion(x, y);
        calculadora.division(x, y);

        //pruebas de la división por 0
        calculadora.division(1,0);
    }

    //apartado 4
    private static void apartado4()
    {
        string cadena = "";
        cadena=pideCadena("Introducir una cadena: ");
        string[] palabrasCadena=cadena.Trim().Split(" ");

        for (int i = palabrasCadena.Length; i >0; i--){
            Console.Write(palabrasCadena[i-1]+" ");
        }
    }

    //apartado 5
    private static void apartado5()
    {
        string cadena = "";
        cadena = pideCadena("Introducir una cadena: ");
        cadena = cadena.ToLower().Replace(".","").Replace(",", "").Replace(" ", "").Replace("á","a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u");

        bool palindromo = espalindromo(cadena);

        if (palindromo)
        {
            Console.WriteLine($"La palabra {cadena} es palíndromo");
        }
        else {
            Console.WriteLine($"La palabra {cadena} no es palíndromo");
        }
    }

    //metodo para validar los palíndromo del apartado 5
    private static bool espalindromo(string cadena) {
        string letraFinal = "";
        string letraInicio = "";

        for (int i = 1; i <= cadena.Length; i++){
            letraInicio = cadena.Substring(i - 1, 1);
            letraFinal = cadena.Substring(cadena.Length - i, 1);

            if (!letraInicio.Equals(letraFinal))
            {
                return false;
            }
        }
        return true;
    }


    //metodos utiles de pide cadenas o enteros
    private static string pideCadena(string mensaje){
        string cadena = "";

        do {
            Console.Write(mensaje);
            try {
                cadena = Console.ReadLine();
                if (cadena.Trim().Length == 0) {
                    throw new Exception("La cadena esta vacia");
                }
            }catch (Exception e){
                Console.WriteLine(e.Message);
            }
        } while (cadena.Trim().Length == 0);

        return cadena.Trim();
    }

    private static int pideEntero(string mensaje){
        int numero = 0;
        string cadena = "";
        bool correcto = false;

        do {
            Console.Write(mensaje);
            try {
                cadena = Console.ReadLine();
                numero = int.Parse(cadena.Trim());
                correcto = true;
            }
            catch (FormatException fe) {
                Console.WriteLine("El valor introducido no es un número. Por favor, vuelva a intentarlo.");
            }
            catch (IOException ioe){
                Console.WriteLine("Error en la entrada de datos");
            }
        } while (!correcto);

        return numero;
    }
}