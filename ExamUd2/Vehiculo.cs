﻿
internal class Vehiculo{
    private string marca;
    private string modelo;

    public Vehiculo(string marca, string modelo) {
        this.marca = marca;
        this.modelo = modelo;
    }

    public string getMarca() { 
        return this.marca;
    }

    public string getModelo()
    {
        return this.modelo;
    }

    virtual public void mostrarInformacion() {
        Console.WriteLine($"Vehiculo:" +
            $"\n\tmarca: {marca}" +
            $"\n\tmodelo: {modelo}");
    }
}

